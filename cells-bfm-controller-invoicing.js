class CellsBfmControllerInvoicing extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior ], Polymer.Element) {

  static get is() {
    return 'cells-bfm-controller-invoicing';
  }

  static get properties() {
    return {};
  }
  navigate(page) {
    window.cells.navigate(page);
  }
  setDataMenu() {
    var menu = [
      {
        'text': this.t('cells-bfm-menu-pg'),
        'page': 'global-position'
      },
      {
        'text': this.t('cells-bfm-menu-invoicing'),
        'page': 'ovInvoicing'
      }
    ],
    var active = this.t('cells-bfm-menu-invoicing');
    this.dispatchEvent(new CustomEvent('menu-items', {detail: menu}));
    this.dispatchEvent(new CustomEvent('active-item-menu', {detail: active}));
  }
  setDataComponentCredentials(){
      this._setDataCredentials();
      this._setDatahelpers();
  }

}

customElements.define(CellsBfmControllerInvoicing.is, CellsBfmControllerInvoicing);
